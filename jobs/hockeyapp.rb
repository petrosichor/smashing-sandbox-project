#!/usr/bin/env ruby
require 'json'
require 'time'
require 'hockeyapp'

@config = YAML.safe_load(File.open('../secrets.yml'))

return ap("Error: Please setup config/hockeyapp.yml") if @config["appIds"].nil?

HockeyApp::Config.configure do |config|
  config.token = ENV['FL_HOCKEY_API_TOKEN'] || @config['api_key']
end

client = HockeyApp.build_client
apps = client.get_apps

SCHEDULER.every '20m', first_in: 0 do |job|
  begin
    @config['app_ids'].each do |id|
      app = apps.find { |a| a.bundle_identifier == id && a.platform == 'iOS' }
      send_data('catch_app', app.versions.first.version.to_i)
    end
  end
end
